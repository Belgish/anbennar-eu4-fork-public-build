DADV_1 = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		culture_group = dwarven
		has_reform = adventurer_reform
		capital_scope = { continent = serpentspine }
	}
	
	DADV_mapping_the_dwarovar = {
		icon = mission_rb_conquer_brittany
		ai = yes
		position = 1
		trigger = {
			mil_power = 100
			years_of_income = 1
		}
		effect = {
			add_mil_power = -100
			add_years_of_income = -1
			create_conquistador = {
				tradition = 40
			}
		}
	}
	
	DADV_ally_in_the_dark = {
		icon = mission_rb_weather_the_reformation
		ai = yes
		position = 2
		required_missions = {
			DADV_mapping_the_dwarovar
		}
		trigger = {
			custom_trigger_tooltip = {
				tooltip = ally_remnant_tooltip
				any_ally = {
					has_country_flag = dwarovar_remnant
				}
			}
		}
		effect = {
			random_known_country = {
				limit = {
					has_country_flag = dwarovar_remnant
					alliance_with = ROOT
				}
				add_trust = {
					who = ROOT
					value = 25
					mutual = yes
				}
			}
		}
	}
}


DADV_2 = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		culture_group = dwarven
		has_reform = adventurer_reform
		capital_scope = { continent = serpentspine }
	}
	
	DADV_defence_forces = {
		icon = mission_rb_unite_the_clans
		ai = yes
		position = 2
		required_missions = {
			DADV_a_hold_reclaimed
		}
		trigger = {
			army_size = 15
		}
		effect = {
			add_country_modifier = {
				name = dadv_defence_forces
				duration = 9125
			}
		}
	}
	
	DADV_never_again = {
		icon = mission_golden_century
		ai = yes
		position = 3
		required_missions = {
			DADV_defence_forces
			DADV_a_hold_repaired
		}
		trigger = {
			capital_scope = {
				fort_level = 3
			}
		}
		effect = {
			capital_scope = {
				add_permanent_province_modifier = {
					name = dadv_solid_wall
					duration = 18250
				}
			}
		}
	}
	
	DADV_ancestors_smile = {
		icon = mission_have_two_subjects
		ai = yes
		position = 4
		required_missions = {
			DADV_never_again
		}
		trigger = {
			religion = ancestor_worship
			at_war_with_religious_enemy = yes
		}
		effect = {
			add_country_modifier = {
				name = dadv_ancestor_smile
				duration = 9125
			}
		}
	}
	
	DADV_fungal_farm = {
		icon = mission_rice_field
		ai = yes
		position = 5
		required_missions = {
			DADV_expanding_the_hold
		}
		trigger = {
			fungi = 3
		}
		effect = {
			random_owned_province = {
				limit = { trade_goods = fungi }
				add_base_production = 1
				add_base_manpower = 1
			}
			random_owned_province = {
				limit = { trade_goods = fungi }
				add_base_production = 1
				add_base_manpower = 1
			}
			random_owned_province = {
				limit = { trade_goods = fungi }
				add_base_production = 1
				add_base_manpower = 1
			}
		}
	}
	
	DADV_beer_hall = {
		icon = mission_modernize_production
		ai = yes
		position = 6
		required_missions = {
			DADV_fungal_farm
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 2
				trade_goods = wine
			}
		}
		effect = {
			capital_scope = {
				add_permanent_province_modifier = {
					name = dadv_beer_hall
					duration = -1
				}
			}
		}
	}
	
	DADV_growing_frontier = {
		icon = mission_unite_home_region
		ai = yes
		position = 7
		required_missions = {
			DADV_going_further
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 2
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
			}
		}
		effect = {
			add_stability_or_adm_power = yes
			add_prestige = 10
		}
	}
	
	DADV_more_for_the_hoard = {
		icon = mission_african_gold
		ai = yes
		position = 8
		required_missions = {
			DADV_going_deeper
		}
		trigger = {
			treasury = 2000
		}
		effect = {
			add_stability_or_adm_power = yes
			add_prestige = 10
		}
	}
	
	DADV_restoring_the_rail = {
		icon = mission_great_trading_houses
		ai = yes
		position = 9
		required_missions = {
			DADV_toward_new_depths
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 10
				has_terrain = dwarven_road
				NOT = { has_province_modifier = dwarovar_rail }
			}
		}
		effect = {
			add_stability_or_adm_power = yes
			add_prestige = 10
		}
	}
}


DADV_3 = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		culture_group = dwarven
		has_reform = adventurer_reform
		capital_scope = { continent = serpentspine }
	}
	
	DADV_a_hold_reclaimed = {
		icon = mission_arabian_fort
		ai = yes
		position = 1
		required_missions = {
		}
		trigger = {
			capital_scope = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
			}
		}
		effect = {
			add_years_of_income = 0.5
			add_prestige = 10
		}
	}
	
	DADV_a_hold_repaired = {
		icon = mission_early_game_buildings
		ai = yes
		position = 2
		required_missions = {
			DADV_a_hold_reclaimed
		}
		trigger = {
			capital_scope = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
				NOT = { has_province_modifier = ruined_hold }
				NOT = { has_province_modifier = infested_hold }
			}
		}
		effect = {
			add_stability_or_adm_power = yes
			add_prestige = 10
		}
	}
	
	DADV_expanding_the_hold = {
		icon = mission_early_modern_university
		ai = yes
		position = 4
		required_missions = {
			DADV_a_hold_repaired
		}
		trigger = {
			capital_scope = {
				has_dwarven_hold_2 = yes
			}
		}
		effect = {
			define_advisor = {
				type = treasurer
				skill = 3
				discount = yes
			}
		}
	}
	
	DADV_going_further = {
		icon = mission_assert_control_over_delhi
		ai = yes
		position = 6
		required_missions = {
			DADV_expanding_the_hold
		}
		trigger = {
			capital_scope = {
				has_dwarven_hold_3 = yes
			}
		}
		effect = {
			if = {
				limit = {
					capital_scope = {
						NOT = { province_has_center_of_trade_of_level = 3 }
					}
				}
				capital_scope = {
					add_center_of_trade_level = 1
				}
			}
			else = {
				capital_scope = {
					add_base_production = 2
				}
				add_prestige = 10
			}
		}
	}
	
	DADV_going_deeper = {
		icon = mission_defeat_jaunpur
		ai = yes
		position = 7
		required_missions = {
			DADV_going_further
		}
		trigger = {
			capital_scope = {
				has_dwarven_hold_4 = yes
			}
		}
		effect = {
			add_country_modifier = {
				name = dadv_growing_economy
				duration = 9125
			}
		}
	}
	
	DADV_toward_new_depths = {
		icon = mission_south_african_bases
		ai = yes
		position = 8
		required_missions = {
			DADV_going_deeper
		}
		trigger = {
			capital_scope = {
				has_dwarven_hold_5 = yes
			}
		}
		effect = {
			capital_scope = {
				add_permanent_province_modifier = {
					name = dadv_growing_population
					duration = 9125
				}
			}
		}
	}
	
	DADV_till_death_do_us_part = {
		icon = mission_gujarati_textiles
		ai = yes
		position = 9
		required_missions = {
			DADV_toward_new_depths
		}
		trigger = {
			capital_scope = {
				has_dwarven_hold_6 = yes
			}
		}
		effect = {
			add_country_modifier = {
				name = dadv_childrens_of_the_dwarovar
				duration = 36500
			}
			custom_tooltip = dadv_childrens_of_the_dwarovar_tooltip
		}
	}
}


DADV_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		culture_group = dwarven
		has_reform = adventurer_reform
		capital_scope = { continent = serpentspine }
	}
	
	DADV_clear_the_way = {
		icon = mission_zambezi_gold
		ai = yes
		position = 3
		required_missions = {
			DADV_a_hold_repaired
		}
		trigger = {
			dip_power = 150
		}
		effect = {
			add_dip_power = -150
			add_country_modifier = {
				name = dadv_clear_the_way
				duration = 9125
			}
		}
	}
	
	DADV_controlled_expansion = {
		icon = mission_high_income
		ai = yes
		position = 4
		required_missions = {
			DADV_clear_the_way
		}
		trigger = {
			adm_power = 300
		}
		effect = {
			add_adm_power = -300
			add_country_modifier = {
				name = dadv_controlled_expansion
				duration = 18250
			}
		}
	}
	
	DADV_resettling_the_serpentspine = {
		icon = mission_settlers_north_america
		ai = yes
		position = 5
		required_missions = {
			DADV_controlled_expansion
			DADV_expanding_the_hold
			DADV_extended_borders
		}
		trigger = {
			dip_power = 150
			adm_power = 300
		}
		effect = {
			add_dip_power = -150
			add_adm_power = -300
			add_country_modifier = {
				name = dadv_resettling_the_serpentspine
				duration = 36500
			}
		}
	}
	
	DADV_exploit_the_caverns = {
		icon = mission_noble_council
		ai = yes
		position = 7
		required_missions = {
			DADV_going_further
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 20
				has_terrain = cavern
			}
		}
		effect = {
			add_stability_or_adm_power = yes
			capital_scope = {
				add_base_production = 1
				add_base_tax = 1
				add_base_manpower = 1
			}
		}
	}
	
	DADV_digging_expert = {
		icon = mission_scholar_officials
		ai = yes
		position = 8
		required_missions = {
			DADV_going_deeper
		}
		trigger = {
			adm_power = 150
			dip_power = 150
			mil_power = 150
		}
		effect = {
			add_adm_power = -150
			add_dip_power = -150
			add_mil_power = -150
			add_country_modifier = {
				name = dadv_digging_expert
				duration = 18250
			}
			custom_tooltip = dadv_digging_expert_tooltip
		}
	}
	
	DADV_mithril_forge = {
		icon = mission_state_manufactories
		ai = yes
		position = 9
		required_missions = {
			DADV_toward_new_depths
		}
		trigger = {
			mithril = 1
		}
		effect = {
			random_owned_province = {
				limit = { trade_goods = mithril }
				add_base_production = 3
			}
		}
	}
}


DADV_5 = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		culture_group = dwarven
		has_reform = adventurer_reform
		capital_scope = { continent = serpentspine }
	}
	
	DADV_extended_borders = {
		icon = mission_extent_yasak_to_the_east
		ai = yes
		position = 4
		required_missions = {
			DADV_clear_the_way
		}
		trigger = {
			num_of_cities = 12
		}
		effect = {
			add_country_modifier = {
				name = dadv_extended_borders
				duration = 9125
			}
		}
	}
}
