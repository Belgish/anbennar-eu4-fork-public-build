
namespace = flavor_sugamber

# Start of the Sugambrian succession crisis
country_event = {
	id = flavor_sugamber.0
	title = flavor_sugamber.0.t
	desc = flavor_sugamber.0.d
	picture = KING_SICK_IN_BED_eventPicture
	
	fire_only_once = yes

	is_triggered_only = yes

	trigger = {
			tag = A48
			NOT = { has_country_flag = A48_succesion_war_Ethelbert }
			NOT = { has_country_flag = A48_succesion_war_Lisolette }
			NOT = { has_country_flag = had_succession_war }
	}

	immediate = {
	}

	option = {
		name = flavor_sugamber.0.a
		country_event = { id = flavor_sugamber.1 days = 1 }
		hidden_effect = {
			random_list = { 
				35 = { 
				set_country_flag = A48_stabbed
				}
				35 = { 
				set_country_flag = A48_poisoned
				}
				30 = {
				set_country_flag = A48_drunken_reveal
				}
			}
		}
	}

	option = {
		name = flavor_sugamber.0.b
		country_event = { id = flavor_sugamber.1 days = 1 }
		hidden_effect = {
			random_list = { 
				35 = { 
				set_country_flag = A48_cold_feet
				}
				35 = { 
				set_country_flag = A48_generic
				}
				30 = {
				set_country_flag = A48_crossbow
				}
			}
		}
	}
}


country_event = {
	id = flavor_sugamber.1
	title = flavor_sugamber.1.t
	desc = {
		trigger = { has_country_flag = A48_stabbed }
		desc = flavor_sugamber.1.d1
	}
	desc = {
		trigger = { has_country_flag = A48_poisoned }
		desc = flavor_sugamber.1.d2
	}
	desc = {
		trigger = { has_country_flag = A48_drunken_reveal }
		desc = flavor_sugamber.1.d3
	}
	desc = {
		trigger = { has_country_flag = A48_cold_feet }
		desc = flavor_sugamber.1.d4
	}
	desc = {
		trigger = { has_country_flag = A48_generic }
		desc = flavor_sugamber.1.d5
	}
	desc = {
		trigger = { has_country_flag = A48_crossbow }
		desc = flavor_sugamber.1.d6
	}
	picture = ASSASSINATION_eventPicture
	
	fire_only_once = yes

	is_triggered_only = yes
	
	option = {
		name = flavor_sugamber.1.a
		trigger = { 
			OR = {
				has_country_flag = A48_stabbed
				has_country_flag = A48_poisoned
				has_country_flag = A48_crossbow
			}
		}
		country_event = { id = flavor_sugamber.2 days = 1 }
		define_ruler = {
			name = "Ethelbert"
			dynasty = "of Rupellion"
			fixed = yes
			adm = 4
			dip = 4
			mil = 5
			age = 40 
		}
		if = {
			limit = {
				OR = {
					has_country_flag = A48_stabbed
					has_country_flag = A48_crossbow
				}
			}
			reduce_stability_or_adm_power = yes
		}
	}

	option = {
		name = flavor_sugamber.1.b
		trigger = { 
			OR = {
				has_country_flag = A48_drunken_reveal
				has_country_flag = A48_cold_feet
				has_country_flag = A48_generic
			}
		}
		country_event = { id = flavor_sugamber.3 days = 1 }
		if = {
			limit = {
				has_country_flag = A48_generic
			}
			reduce_stability_or_adm_power = yes
		}
	}
}

country_event = {
	id = flavor_sugamber.2
	title = flavor_sugamber.2.t
	desc = flavor_sugamber.2.d
	picture = CIVIL_WAR_eventPicture
	
	fire_only_once = yes
	major = yes

	is_triggered_only = yes

	immediate = {
	}

	option = {
		name = flavor_sugamber.2.a
		if = {
			limit = {
				owns = 415		# Lockcastle
				415 = {
					units_in_province = 1
				}
			}
			415 = {
				set_province_flag = local_support_for_rebells 
				spawn_rebels = {
					type = pretender_rebels
					size = 1.8
					leader = "Baldwin of Lockcastle"
					win = yes
				}
			}
		}
		else = {
			random_owned_province = {
				limit = {
					NOT = { units_in_province = 1 }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				set_province_flag = local_support_for_rebells
				spawn_rebels = {
					type = pretender_rebels
					size = 1.8
					leader = "Baldwin of Lockcastle"
					win = yes
				}
			}
		}
		random_owned_province = {
			limit = {
				NOT = { units_in_province = 1 }
				NOT = { has_province_modifier = gnollish_minority_coexisting_large}
				NOT = { culture = hill_gnoll }
			}
			set_province_flag = local_support_for_rebells
			spawn_rebels = {
				type = pretender_rebels
				size = 1.2
				leader = "Baldwin of Lockcastle"
				win = yes
			}
		}
		add_yearly_manpower = -2
		set_country_flag = A48_succesion_war_Ethelbert
	}
}

# Ethelbert rises up
country_event = {
	id = flavor_sugamber.3
	title = flavor_sugamber.3.t
	desc = flavor_sugamber.3.d
	picture = CIVIL_WAR_eventPicture
	
	fire_only_once = yes
	major = yes

	is_triggered_only = yes

	immediate = {
	}

	option = {
		name = flavor_sugamber.3.a
			random_owned_province = {
				limit = {
					NOT = { units_in_province = 1 }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				set_province_flag = local_support_for_rebells
				spawn_rebels = {
					type = pretender_rebels
					size = 1.8
					leader = "Ethelbert of Rupellion"
					win = yes
				}
			}
			random_owned_province = {
				limit = {
					NOT = { units_in_province = 1 }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				set_province_flag = local_support_for_rebells
				spawn_rebels = {
					type = pretender_rebels
					size = 1.2
					leader = "Ethelbert of Rupellion"
					win = yes
				}
			}
			add_yearly_manpower = -2
			set_country_flag = A48_succesion_war_Lisolette
	}
}

# Alternative start of the Sugambrian succession crisis
country_event = {
	id = flavor_sugamber.4
	title = flavor_sugamber.4.t
	desc = flavor_sugamber.4.d
	picture = DEATH_OF_HEIR_eventPicture
	
	fire_only_once = yes
	major = yes

	is_triggered_only = yes

	trigger = {
		tag = A48
		NOT = { has_country_flag = A48_succesion_war_Ethelbert }
		NOT = { has_country_flag = A48_succesion_war_Lisolette }
		NOT = { has_country_flag = had_succession_war }
	}

	immediate = {
	}

	option = {
		name = flavor_sugamber.4.a
			reduce_stability_or_adm_power = yes
			define_heir = {
				name = "Baldwin"
				dynasty = "Lockcastle"
				#fixed = yes
				adm = 4
				dip = 3
				mil = 4
				age = 39
			}
			random_owned_province = {
				limit = {
					NOT = { units_in_province = 1 }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				set_province_flag = local_support_for_rebells
				spawn_rebels = {
					type = pretender_rebels
					size = 1.8
					leader = "Ethelbert of Rupellion"
					win = yes
				}
			}
			random_owned_province = {
				limit = {
					NOT = { units_in_province = 1 }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				set_province_flag = local_support_for_rebells
				spawn_rebels = {
					type = pretender_rebels
					size = 1.2
					leader = "Ethelbert of Rupellion"
					win = yes
				}
			}
			add_yearly_manpower = -2
			set_country_flag = A48_succesion_war_Lisolette
	}
}

# Gnolles might start making trouble
country_event = {
	id = flavor_sugamber.5
	title = flavor_sugamber.5.t
	desc = flavor_sugamber.5.d
	picture = REVOLUTION_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = A48
		OR	= {	has_country_flag = A48_succesion_war_Ethelbert
				has_country_flag = A48_succesion_war_Lisolette
		}
		owns = 913
		controls = 913
	}
	
	mean_time_to_happen = {
		months = 9
	}
	
	immediate = {
	}
	
	option = {	# Just ignore them.
		name = flavor_sugamber.5.a
		random = { 
			chance = 50 
			913 = {
			spawn_rebels = {
				type = nationalist_rebels
				size = 1
				friend = A49
				}
			}
		}
	}
	
	option = {	# Pre-empt rebelion by giving them someone else to fight.
		name = flavor_sugamber.5.b
			913 = {
				add_unit_construction = {
					type = infantry
					amount = 1
					speed = 0.25
					cost = 1
					mercenary = yes
			}
		}
		set_country_flag = gnollish_officer	
	}
	
	option = {	# Keep them quit by lowering their taxes.
		name = flavor_sugamber.5.c
			913 = {
				add_local_autonomy = 30
		}	
	}
}

# The civil war hurts our agriculture.
country_event = {
	id =  flavor_sugamber.6
	title = flavor_sugamber.6.t
	desc = flavor_sugamber.6.d
	picture = FAMINE_eventPicture
	
	trigger = {
		tag = A48
		OR	= {	has_country_flag = A48_succesion_war_Ethelbert
				has_country_flag = A48_succesion_war_Lisolette
		}
	}
	
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {		# Why can't the peasants eat cake?
		name = flavor_sugamber.6.a
		ai_chance = { factor = 25 }
		reduce_stability_or_adm_power = yes
		reduce_legitimacy_small_effect = yes
	}
	option = {		# Just import food from abroad.
		name = flavor_sugamber.6.b
		ai_chance = { factor = 50 }
		add_years_of_income = -0.5
	}
	option = {		# Someone must be hoarding food, we must seize it.
		name = flavor_sugamber.6.c
		ai_chance = { factor = 25 }
		reduce_estate_nobles_loyalty_effect = yes
		reduce_estate_burghers_loyalty_effect = yes
		else = {
			add_adm_power = -100
			add_dip_power = -50
			reduce_legitimacy_small_effect = yes
		}
	}
}

#Neighbors supporting Lisolette
country_event = {
	id = flavor_sugamber.10
	title = flavor_sugamber.10.t
	desc = flavor_sugamber.10.d
	picture = SPY_eventPicture
	
	trigger = {
		is_neighbor_of = A48
		A48 = {
			has_country_flag = A48_succesion_war_Ethelbert
		}
		NOT = { has_country_flag = support_rebels }
		NOT = { has_country_flag = no_support_for_rebels }
		A48 = {
			any_owned_province = {
				OR = {
					any_neighbor_province = {
						owned_by = ROOT
					}
					has_port = yes
				}
				unit_in_siege = no
				NOT = { has_province_flag = local_support_for_rebells }
				NOT = { has_province_modifier = gnollish_minority_coexisting_large}
				NOT = { culture = hill_gnoll }
			}
		}
	}
	
	mean_time_to_happen = {
		months = 18
	}
	
	option = {		# Send some unruly subjects to the rebels as 'volunteers'.
		name = flavor_sugamber.10.a
		ai_chance = { factor = 30 }
		set_country_flag = support_rebels
		add_manpower = -2
		hidden_effect = {
			A48 = {
				country_event = { id = flavor_sugamber.12 days = 1 }
			}
		}
		reverse_add_opinion = {
				who = A48
				modifier = A48_supported_rebels
		}
	}
	option = {		# Give the rebels some monetary aid.
		name = flavor_sugamber.10.b
		ai_chance = { factor = 20 }
		set_country_flag = support_rebels
		add_years_of_income = -0.4
		hidden_effect = {
			A48 = {
				country_event = { id = flavor_sugamber.12 days = 1 }
			}
		}
		reverse_add_opinion = {
				who = A48
				modifier = A48_supported_rebels
		}
	}
	option = {		# We don't help rebel scum.
		name = flavor_sugamber.10.c
		ai_chance = { 
			factor = 50
			modifier = {
				factor = 3
				has_opinion = {
					who = A48
					value = 30 }
			}
			modifier = {
				factor = 2
				NOT = { tag = A56 } #Celliande
				NOT = { tag = A34 } #Bisan
			}
			modifier = {
				factor = 0.3
				NOT = {
					has_opinion = {
						who = A48
						value = -30 }
				}
			}
		}
		set_country_flag = no_support_for_rebels
		add_prestige = 5
		reverse_add_opinion = {
				who = A48
				modifier = A48_didnt_support_rebels
		}
	}
}

#Neighbors supporting Ethelbert
country_event = {
	id = flavor_sugamber.11
	title = flavor_sugamber.11.t
	desc = flavor_sugamber.11.d
	picture = SPY_eventPicture
	
	trigger = {
		is_neighbor_of = A48
		A48 = {
			has_country_flag = A48_succesion_war_Lisolette
		}
		NOT = { has_country_flag = support_rebels }
		NOT = { has_country_flag = no_support_for_rebels }
		A48 = {
			any_owned_province = {
				OR = {
					any_neighbor_province = {
						owned_by = ROOT
					}
					has_port = yes
				}
				unit_in_siege = no
				NOT = { has_province_flag = local_support_for_rebells }
				NOT = { has_province_modifier = gnollish_minority_coexisting_large}
				NOT = { culture = hill_gnoll }
			}
		}
	}
	
	mean_time_to_happen = {
		months = 18
	}
	
	option = {		# Send some unruly subjects to the rebels as 'volunteers'.
		name = flavor_sugamber.11.a
		ai_chance = { factor = 30 }
		set_country_flag = support_rebels
		add_manpower = -2
		reverse_add_opinion = {
				who = A48
				modifier = A48_supported_rebels
		}
		hidden_effect = {
			A48 = {
				country_event = { id = flavor_sugamber.12 days = 1 }
			}
		}
	}
	option = {		# Give the rebels some monetary aid.
		name = flavor_sugamber.11.b
		ai_chance = { factor = 20 }
		set_country_flag = support_rebels
		add_years_of_income = -0.4
		reverse_add_opinion = {
				who = A48
				modifier = A48_supported_rebels
		}
		hidden_effect = {
			A48 = {
				country_event = { id = flavor_sugamber.12 days = 1 }
			}
		}
	}
	option = {		# We don't help rebel scum.
		name = flavor_sugamber.11.c
		ai_chance = { 
			factor = 50
			modifier = {
				factor = 3
				has_opinion = {
					who = A48
					value = 30 }
			}
			modifier = {
				factor = 0.3
				NOT = {
					has_opinion = {
						who = A48
						value = -30 }
				}
			}
		}
		set_country_flag = no_support_for_rebels
		add_prestige = 5
		reverse_add_opinion = {
				who = A48
				modifier = A48_didnt_support_rebels
		}
	}
}

# Country Intervenes!
country_event = {
	id = flavor_sugamber.12
	title = flavor_sugamber.12.t
	desc = flavor_sugamber.12.d
	picture = SPY_eventPicture
	
	is_triggered_only = yes
	
	option = {		# We will not forget this.
		name = flavor_sugamber.12.a
		if = {
			limit = {
				any_owned_province = {
					any_neighbor_province = {
						owned_by = FROM
					}
					unit_in_siege = no
				}
			}
			random_owned_province = {
				limit = {
					OR = {
						any_neighbor_province = {
							owned_by = ROOT
						}
					has_port = yes
					}
					unit_in_siege = no
					NOT = { has_province_flag = local_support_for_rebells }
					NOT = { has_province_modifier = gnollish_minority_coexisting_large}
					NOT = { culture = hill_gnoll }
				}
				spawn_rebels = {
					type = pretender_rebels
					size = 0.8
					friend = FROM
				}
				set_province_flag = local_support_for_rebells
			}
		}
	}
}


########################################
# The End of the War
########################################

# Rebels beaten
country_event = {
	id = flavor_sugamber.13
	title = flavor_sugamber.13.t
	desc = flavor_sugamber.13.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	major = yes
	fire_only_once = yes
	
	mean_time_to_happen = {
		months = 1
	}
	
	trigger = {
		tag = A48
		NOT = { has_country_flag = had_succession_war }
		NOT = { num_of_rebel_controlled_provinces = 1 }
		NOT = { num_of_rebel_armies = 1 }
		OR	= {	has_country_flag = A48_succesion_war_Ethelbert
				has_country_flag = A48_succesion_war_Lisolette
		}
	}
	
	immediate = {
		hidden_effect = {
				
			# Clear Flags
			clr_country_flag = A48_succesion_war_Ethelbert
			clr_country_flag = A48_succesion_war_Lisolette
			
			# Set Flag
			set_country_flag = had_succession_war
		}
	}
	
	option = {		# Order has finally been restored
		name = flavor_sugamber.13.a
		add_stability_or_adm_power = yes
		add_prestige = 10
		every_owned_province = {
			limit = {
				has_province_flag = local_support_for_rebells
			}
			add_province_modifier = {
				name = add_unrest_5_modifier
				duration = 3650
			}
			clr_province_flag = local_support_for_rebells
		}
		hidden_effect = {
			every_country = {
				limit = {
					OR = {
						has_country_flag = no_support_for_rebels
						has_country_flag = support_rebels
					}
				}
				country_event = { id = flavor_sugamber.17 days = 1 } #Relations deteriorate with Sugamber
			}
			if = { limit = { has_country_flag = gnollish_officer }
				country_event = { id = flavor_sugamber.18 days = 60 }
			}
		}
	}
}

# Pro-Lisolette rebels win the war
country_event = {
	id = flavor_sugamber.14
	title = flavor_sugamber.14.t
	desc = flavor_sugamber.14.d
	picture = REVOLUTION_eventPicture

	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		tag = A48
		has_country_flag = A48_succesion_war_Ethelbert
		NOT = { dynasty = "of Rupellion" }
	}
	
	immediate = {
		hidden_effect = {
			# Clear Flags
			clr_country_flag = A48_succesion_war_Ethelbert
			clr_country_flag = A48_succesion_war_Lisolette
			
			# Set Flag
			set_country_flag = had_succession_war
		}
	}
	
	option = {		# Lisolette has been avenged!
		name = flavor_sugamber.14.a
		add_stability_or_adm_power = yes
		add_prestige = -10
		every_owned_province = {
			limit = {
					NOT = { has_province_flag = local_support_for_rebells }
			}
			add_province_modifier = {
				name = add_unrest_5_modifier
				duration = 3650
			}
		}
		if = { limit = { A34 = { has_country_flag = support_rebels } }
			country_event = { id = flavor_sugamber.20 days = 30 } # Returning territory to Bisan?
		}
		if = { limit = { A56 = { has_country_flag = support_rebels } }
			country_event = { id = flavor_sugamber.21 days = 30 } # Returning territory to Celliande?
		}
		hidden_effect = {
			# Clear Province Flags
			every_owned_province = {
				clr_province_flag = local_support_for_rebells
			}
			every_country = {
				limit = {
					has_country_flag = support_rebels
				}
				country_event = { id = flavor_sugamber.16 days = 1 } # Improved Relations with Sugamber
			}
			every_country = {
				limit = {
					has_country_flag = no_support_for_rebels
				}
				remove_opinion = {
					who = ROOT
					modifier = A48_didnt_support_rebels
				}
				clr_country_flag = no_support_for_rebels
			}
			if = { limit = { has_dlc = "The Cossacks" }
				country_event = { id = flavor_sugamber.19 days = 60 } # To the victor's supporters go the spoils
			}
		}
	}
}

# Pro-Ethelbert rebels win the war
country_event = {
	id = flavor_sugamber.15
	title = flavor_sugamber.15.t
	desc = flavor_sugamber.15.d
	picture = REVOLUTION_eventPicture
	
	major = yes
	is_triggered_only = yes
	fire_only_once = yes
	
	trigger = {
		tag = A48
		has_country_flag = A48_succesion_war_Lisolette
		NOT = { dynasty = "síl Rhinmond" }
	}
	
	immediate = {
		hidden_effect = {
				
			# Clear Flags
			clr_country_flag = A48_succesion_war_Ethelbert
			clr_country_flag = A48_succesion_war_Lisolette
			
			# Set Flag
			set_country_flag = had_succession_war
		}
	}
	
	option = {		# Justice has been fullfilled!
		name = flavor_sugamber.15.a
		add_stability_or_adm_power = yes
		add_prestige = -10
		every_owned_province = {
			limit = {
					NOT = { has_province_flag = local_support_for_rebells }
			}
			add_province_modifier = {
				name = add_unrest_5_modifier
				duration = 3650
			}
		}
		if = { 
			limit = { 
				NOT = { war_with = A34 } 
				A34 = { has_country_flag = support_rebels }
			}
			country_event = { id = flavor_sugamber.20 days = 30 } # Returning territory to Bisan?
		}
		if = { 
			limit = { 
				NOT = { war_with = A56 } 
				A56 = { has_country_flag = support_rebels } 
			}
			country_event = { id = flavor_sugamber.21 days = 30 } # Returning territory to Celliande?
		}
		hidden_effect = {
			# Clear Province Flags
			every_owned_province = {
				clr_province_flag = local_support_for_rebells
			}
			every_country = {
				limit = {
					has_country_flag = support_rebels
				}
				country_event = { id = flavor_sugamber.16 days = 1 } # Improved Relations with Sugamber
			}
			every_country = {
				limit = {
					has_country_flag = no_support_for_rebels
				}
				remove_opinion = {
					who = ROOT
					modifier = A48_didnt_support_rebels
				}
				clr_country_flag = no_support_for_rebels
			}
			if = { limit = { has_dlc = "The Cossacks" }
				country_event = { id = flavor_sugamber.19 days = 60 } # To the victor's supporters go the spoils
			}
		}
	}
}

# Improved Relations with Sugamber
country_event = {
	id = flavor_sugamber.16
	title = flavor_sugamber.16.t
	desc = flavor_sugamber.16.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		clr_country_flag = support_rebels
	}
	
	option = {		# Hopefully he'll one day pays us back.
		name = flavor_sugamber.16.a
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = A48_support_during_succession_war
			}
			remove_opinion = {
				who = ROOT
				modifier = A48_supported_rebels
			}
		}
	}
}

# Relations Deteriorate with Sugamber
country_event = {
	id = flavor_sugamber.17
	title = flavor_sugamber.17.t
	desc = flavor_sugamber.17.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	option = {		# Not mentioning any names? that's how you recognize anyone who's bluffing.
		trigger = { has_country_flag = support_rebels }
		name = flavor_sugamber.17.a
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = A48_supported_rebels
			}
		}
		clr_country_flag = support_rebels
	}
	
	option = {		# Oh, good thing we didn't.
		trigger = { has_country_flag = no_support_for_rebels }
		name = flavor_sugamber.17.b
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = A48_didnt_support_rebels
			}
		}
		clr_country_flag = no_support_for_rebels
	}
}

# Gnollish officers?
country_event = {
	id = flavor_sugamber.18
	title = flavor_sugamber.18.t
	desc = flavor_sugamber.18.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	is_triggered_only = yes
	
	
	immediate = {
		hidden_effect = {
			clr_country_flag = gnollish_officer
		}
	}

	option = {	# Give him a high function.
		name = flavor_sugamber.18.a
		ai_chance = { factor = 10 }
		define_advisor = {
			type = commandant
			skill = 1
			culture = hill_gnoll
			location = 913
			discount = yes
		}
		reduce_legitimacy_medium_effect = yes
		add_prestige = -10
		add_army_tradition = 5
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -5
		}
		add_estate_loyalty = {
			estate = estate_church
			loyalty = -5
		}
		316 = { 
			remove_province_modifier = gnollish_minority_coexisting_large
			add_permanent_province_modifier = {
				name = gnollish_minority_integrated_large
				duration = -1
			}
		}
	}

	option = {	# Make him an officer.
		name = flavor_sugamber.18.b
		ai_chance = { factor = 50 }
		add_army_tradition = 5
	}

	option = {	# Gnolles already cause enough havoc without recieving high positions.
		name = flavor_sugamber.18.a
		ai_chance = { factor = 40 }
		increase_legitimacy_small_effect = yes
		add_prestige = 5
		add_army_tradition = -5
		316 = { 
			remove_province_modifier = gnollish_minority_coexisting_large
			add_permanent_province_modifier = {
				name = gnollish_minority_oppressed_large
				duration = -1
			}
		}
	}
}

# To the victor's supporters go the spoils
country_event = {
	id = flavor_sugamber.19
	title = flavor_sugamber.19.t
	desc = flavor_sugamber.19.d
	picture = GOOD_WITH_MONARCH_eventPicture
	
	fire_only_once = yes

	is_triggered_only = yes
	
	trigger = {
		tag = A48
	}

	option = { # Divide the crown lawns under our supporters.
		name = flavor_sugamber.19.a
		ai_chance = { factor = 20 }
		random_owned_province = {
			limit = {
				has_estate = no
			}
			set_estate = estate_nobles
			add_local_autonomy = 40
		}
		random_owned_province = {
			limit = {
				has_estate = no
			}
			set_estate = estate_nobles
			add_local_autonomy = 40
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 5
		}
	}

	option = { # Arrange for public offices to be given to their surplus children, even if those traditionaly went to priest or burghers.
		name = flavor_sugamber.19.b
		ai_chance = { factor = 20 }
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = -5
		}
		add_estate_loyalty = {
			estate = estate_church
			loyalty = -5
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 5
		}
		add_corruption = 5
	}

	option = { # Give them a donation out of the treasury.
		name = flavor_sugamber.19.c
		ai_chance = {
			factor = 30
			modifier = {
				factor = 0.1
				num_of_loans = 2
			}
		}
		add_years_of_income = -0.75
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = 5
		}
	}

	option = { # They only did their duty, I don't reward basic decency.
		name = flavor_sugamber.19.e
		ai_chance = {
			factor = 30
			modifier = {
				factor = 0.1
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 40
					}
				}
			}
		}
		reduce_estate_nobles_loyalty_effect = yes
	}
}

# Returning territory to Bisan?
country_event = {
	id = flavor_sugamber.20
	title = flavor_sugamber.20.t
	desc = flavor_sugamber.20.d
	picture = MERCHANTS_TALKING_eventPicture

	is_triggered_only = yes

	option = { # They can have Gnollesgate, the gnolles only makes trouble anyway.
		name = flavor_sugamber.20.a
		trigger = { owns = 913 }
		ai_chance = { factor = 50 }
		913 = {
			cede_province = A34
			remove_core = A48
		}
	}

	option = { # Give them Countsbridge.
		name = flavor_sugamber.20.b
		trigger = { owns = 315 }
		ai_chance = { factor = 20 }
		315 = {
			cede_province = A34
			remove_core = A48
		}
		A34 = {
			add_opinion = {
				who = ROOT
				modifier = A48_gave_important_province
			}
		}
	}

	option = { # Fullfill all their claims.
		name = flavor_sugamber.20.c
		trigger = { 
			owns = 913
			owns = 315
		}
		ai_chance = { factor = 10 }
		913 = {
			cede_province = A34
			remove_core = A48
		}
		315 = {
			cede_province = A34
			remove_core = A48
		}
		A34 = {
			add_historical_friend = A48
			add_opinion = {
				who = ROOT
				modifier = A48_gave_important_province
			}
		}
	}

	option = { # Pay them in cash.
		name = flavor_sugamber.20.e
		ai_chance = {
			factor = 10
			modifier = {
				factor = 0.1
				num_of_loans = 2
			}
			modifier = {
				factor = 3
				NOT = { owns = 913 }
			}
			modifier = {
				factor = 3
				NOT = { owns = 315 }
			}
		}
		add_years_of_income = -1
		A34 = {
			country_event = { id = flavor_sugamber.22 days = 1 } # We recieved tribute from Sugamber
		}
	}

	option = { # Haha! did they really thought we'd pay them back.
		name = flavor_sugamber.20.f
		ai_chance = { factor = 20 }
		A34 = {
			add_historical_rival = A48
			add_opinion = {
				who = ROOT
				modifier = A48_didnt_paid_us_back
			}
		}
	}
}

# Returning territory to Celliande?
country_event = {
	id = flavor_sugamber.21
	title = flavor_sugamber.21.t
	desc = flavor_sugamber.21.d
	picture = MERCHANTS_TALKING_eventPicture

	is_triggered_only = yes

	option = { # They can have Hawkshot, we still have a coastline without it.
		name = flavor_sugamber.21.a
		trigger = { owns = 410 }
		ai_chance = { factor = 50 }
		410 = {
			cede_province = A56
			remove_core = A48
		}
	}

	option = { # Give them Irmathmas, if we haven't got any port we can't get raided by pirates.
		name = flavor_sugamber.21.b
		trigger = { owns = 411 }
		ai_chance = { factor = 21 }
		411 = {
			cede_province = A56
			remove_core = A48
		}
		A56 = {
			add_opinion = {
				who = ROOT
				modifier = A48_gave_important_province
			}
		}
	}

	option = { # Fullfill all their claims.
		name = flavor_sugamber.21.c
		trigger = { 
			owns = 410
			owns = 411
		}
		ai_chance = { factor = 10 }
		410 = {
			cede_province = A56
			remove_core = A48
		}
		411 = {
			cede_province = A56
			remove_core = A48
		}
		A56 = {
			add_historical_friend = A48
			add_opinion = {
				who = ROOT
				modifier = A48_gave_important_province
			}
		}
	}

	option = { # Pay them in cash.
		name = flavor_sugamber.21.e
		ai_chance = {
			factor = 10
			modifier = {
				factor = 0.1
				num_of_loans = 2
			}
			modifier = {
				factor = 3
				NOT = { owns = 410 }
			}
			modifier = {
				factor = 3
				NOT = { owns = 411 }
			}
		}
		add_years_of_income = -1
		A56 = {
			country_event = { id = flavor_sugamber.22 days = 1 } # We recieved tribute from Sugamber
		}
	}

	option = { # Haha, did they really thought we'd pay them back
		name = flavor_sugamber.21.f
		ai_chance = { factor = 21 }
		A56 = {
			add_historical_rival = A48
			add_opinion = {
				who = ROOT
				modifier = A48_didnt_paid_us_back
			}
		}
	}
}

# We recieved tribute from Sugamber
country_event = {
	id = flavor_sugamber.22
	title = flavor_sugamber.22.t
	desc = flavor_sugamber.22.d
	picture = MERCHANTS_TALKING_eventPicture
	major = yes
	is_triggered_only = yes

	option = { # They are lucky that it now would appear unthankfull to attack them
		name = flavor_sugamber.22.a
		add_years_of_income = 1.5
	}
}