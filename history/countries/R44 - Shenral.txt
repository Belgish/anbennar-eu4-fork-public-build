government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = raghamidesh
religion = high_philosophy
technology_group = tech_raheni
religious_school = starry_eye_school
capital = 4368

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Ghoshar"
		dynasty = "of the Vengeful Gaze"
		birth_date = 1421.4.1
		adm = 3
		dip = 3
		mil = 4
		culture = royal_harimari
	}
}